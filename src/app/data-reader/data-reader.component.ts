import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {DataReaderService} from "../service/data-reader.service";
import {Scene} from "../model/scene";

@Component({
  selector: 'gk-data-reader',
  templateUrl: './data-reader.component.html',
  styleUrls: ['./data-reader.component.scss']
})
export class DataReaderComponent implements OnInit {

  @Output()
  dataChange: EventEmitter<Scene> = new EventEmitter<Scene>();

  constructor() { }

  ngOnInit() {
  }

  loadData(): void {
    this.dataChange.next(DataReaderService.read());
  }

}

