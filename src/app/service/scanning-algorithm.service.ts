import { Injectable } from '@angular/core';
import {Constants} from "../model/constants";
import {Point2D} from "../model/point2-d";
import {Wall} from "../model/wall";
import {Camera} from "../model/camera";
import {Scene} from "../model/scene";
import {Line3D} from "../model/line3-d";
import {LineService} from "./line.service";
import {CanvasLine} from "../model/canvas-line";
import {WallService} from "./wall.service";
import {Color} from "../model/color.enum";

@Injectable({
  providedIn: 'root'
})
export class ScanningAlgorithmService {
  scene: Scene;
  camera: Camera;

  minY: number;
  maxY: number;

  intersectionPoints: Array<Point2D>;
  canvasLines: Array<CanvasLine>;

  constructor() { }

  public setValues(scene: Scene, camera: Camera): void {
    this.scene = scene;
    this.camera = camera;
    this.intersectionPoints = [];
    this.canvasLines = [];
  }

  public calculate(): Array<CanvasLine> {
    this.calculateMinYAndMaxY();
    for (let y = this.minY; y <= this.maxY; y++) {
      this.calculateIntersectionsPoints(y);
      this.createCanvasLines(y);
    }
    return this.canvasLines;
  }

  /**
   * wylicza min y i max y, dla którego będzie coś rysowane
   */
  private calculateMinYAndMaxY(): void {
    let minY = LineService.getMinY(this.scene.shapes[0].lines[0], this.camera);
    let maxY = LineService.getMaxY(this.scene.shapes[0].lines[0], this.camera);
    for (let i: number = 0; i < this.scene.shapes.length; i++) {
      for (let j: number = 0; j < this.scene.shapes[i].lines.length; j++) {
        let currentMinY: number = LineService.getMinY(this.scene.shapes[i].lines[j], this.camera);
        if (currentMinY < minY) {
          minY = currentMinY;
        }
        let currentMaxY: number = LineService.getMaxY(this.scene.shapes[i].lines[j], this.camera);
        if (currentMaxY > maxY) {
          maxY = currentMaxY;
        }
      }
    }
    this.minY = minY > - Constants.HEIGHT / 2 ? minY : - Constants.HEIGHT / 2;
    this.maxY = maxY < Constants.HEIGHT / 2 ? maxY: Constants.HEIGHT / 2;
  }

  /**
   * wylicza dla danego y punkty przecięć
   */
  private calculateIntersectionsPoints(height: number): void {
    this.intersectionPoints = [];
    let line: Line3D;

    for (let i: number = 0; i < this.scene.shapes.length; i++) {
      for (let j: number = 0; j < this.scene.shapes[i].lines.length; j++) {
        line = this.scene.shapes[i].lines[j];
        if (LineService.isIntersection(line, this.camera, height)) {
          let intersection: number = LineService.getIntersectionX(line, this.camera, height);
          if (intersection !== -Infinity && intersection !== Infinity && !isNaN(intersection)) {
            let point: Point2D = new Point2D(intersection, height);
            this.intersectionPoints.push(point);
          }
        }
      }
    }
    if (this.intersectionPoints.length != 0) {
      this.sortIntersectionPoints();
    }
  }

  /**
   * posortowanie punktów asc x
   */
  private sortIntersectionPoints(): void {
    this.intersectionPoints.sort(function (a: Point2D, b: Point2D) { return a.x-b.x });
  }

  /**
   * dodanie linii canvas
   */
  private createCanvasLines(y: number): void {
    if (this.intersectionPoints.length != 0) {
      for (let i: number = 0; i < this.intersectionPoints.length - 1; i++) {
        let walls = [];
        let middlePoint: Point2D = ScanningAlgorithmService.calculateMiddlePoint(this.intersectionPoints[i], this.intersectionPoints[i+1]);
        for (let j: number = 0; j < this.scene.shapes.length; j++) {
          for (let k: number = 0; k < this.scene.shapes[j].walls.length; k++) {
            if (WallService.isPointInWallRange(this.scene.shapes[j].walls[k], middlePoint, this.camera)) {
              walls.push(this.scene.shapes[j].walls[k]);
            }
          }
        }
        if (walls.length != 0) {
          this.canvasLines.push(new CanvasLine(this.intersectionPoints[i].x, this.intersectionPoints[i+1].x, y, this.getNearestWallColor(walls, middlePoint)));
        }
      }
    }
  }

  /**
   * wylicza srodkowy punkt pomiędzy punktami
   */
  private static calculateMiddlePoint(start: Point2D, end: Point2D): Point2D {
    return new Point2D((start.x + end.x) / 2, (start.y + end.y) / 2);
  }

  /**
   * wylicza kolor
   */
  private getNearestWallColor(walls: Array<Wall>, middlePoint: Point2D): Color {
    let nearestWall: Wall = walls[0];
    let minZ: number = this.countDistance(nearestWall, middlePoint);
    walls.forEach(w => {
      let distance: number = this.countDistance(w, middlePoint);
      if (distance < minZ) {
        minZ = distance;
        nearestWall = w;
      }
    });
    return nearestWall.color;
  }

  private countDistance(wall: Wall, point: Point2D): number {
    return - (wall.A / wall.B * point.x) - (wall.C * point.y / wall.B) - (wall.D / wall.B);
  }

}

