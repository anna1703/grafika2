import { Injectable } from '@angular/core';
import {Line3D} from "../model/line3-d";
import {Camera} from "../model/camera";
import {Line2D} from "../model/line2-d";
import {PerspectiveViewService} from "./perspective-view.service";
import {Constants} from "../model/constants";

@Injectable({
  providedIn: 'root'
})
export class LineService {

  constructor() { }

  /**
   * zwraca, czy rzut linii 3d przecina się z poziomą linią y
   */
  public static isIntersection(line: Line3D, camera: Camera, y: number): boolean {
    let line2D: Line2D = PerspectiveViewService.transformLine(line, camera);
    return y >= Math.min(line2D.startPoint.y, line2D.endPoint.y) &&
           y <= Math.max(line2D.startPoint.y, line2D.endPoint.y);
  }

  /**
   * zwraca wspolrzedna x przeciecia
   */
  public static getIntersectionX(line: Line3D, camera: Camera, y: number): number {
    let line2D: Line2D = PerspectiveViewService.transformLine(line, camera);
    let temp: number = (line2D.startPoint.x - line2D.endPoint.x) / (line2D.startPoint.y - line2D.endPoint.y);
    return temp * y + line2D.startPoint.x - temp * line2D.startPoint.y;
  }

  public static isVisible(line: Line3D, camera: Camera): boolean {
    let line2D: Line2D = PerspectiveViewService.transformLine(line, camera);
    let isWidthOk: boolean = (line2D.startPoint.x >= -Constants.WIDTH/2 && line2D.startPoint.x <= Constants.WIDTH/2)
                          || (line2D.endPoint.x >= -Constants.WIDTH/2 && line2D.endPoint.x <= Constants.WIDTH/2);
    let isHeightOk: boolean = (line2D.startPoint.y >= -Constants.HEIGHT/2 && line2D.startPoint.y <= Constants.HEIGHT/2)
                          || (line2D.endPoint.y >= -Constants.HEIGHT/2 && line2D.endPoint.y <= Constants.HEIGHT/2);
    return isWidthOk && isHeightOk;
  }

  public static getMinY(line: Line3D, camera: Camera): number {
    let line2D: Line2D = PerspectiveViewService.transformLine(line, camera);
    return Math.min(line2D.startPoint.y, line2D.endPoint.y);
  }

  public static getMaxY(line: Line3D, camera: Camera): number {
    let line2D: Line2D = PerspectiveViewService.transformLine(line, camera);
    return Math.max(line2D.startPoint.y, line2D.endPoint.y);
  }

}
