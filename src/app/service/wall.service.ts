import { Injectable } from '@angular/core';
import {Point2D} from "../model/point2-d";
import {LineService} from "./line.service";
import {Wall} from "../model/wall";
import {Camera} from "../model/camera";

@Injectable({
  providedIn: 'root'
})
export class WallService {

  constructor() {
  }

  /**
   * czy punkt nalezy do sciany (z punktu widzenia obserwatora)
   */
  public static isPointInWallRange(wall: Wall, point2D: Point2D, camera: Camera): boolean {
    let x: number = point2D.x;
    let y: number = point2D.y;
    let ix: Array<number> = [];

    let isIntersection1: boolean = LineService.isIntersection(wall.lines[0], camera, y);
    let isIntersection2: boolean = LineService.isIntersection(wall.lines[1], camera, y);
    let isIntersection3: boolean = LineService.isIntersection(wall.lines[2], camera, y);
    let isIntersection4: boolean = LineService.isIntersection(wall.lines[3], camera, y);
    if (!isIntersection1 && !isIntersection2 && !isIntersection3 && !isIntersection4) {
      return false;
    }
    if (isIntersection1) {
      ix.push(LineService.getIntersectionX(wall.lines[0], camera, y));
    }
    if (isIntersection2) {
      ix.push(LineService.getIntersectionX(wall.lines[1], camera, y));
    }
    if (isIntersection3) {
      ix.push(LineService.getIntersectionX(wall.lines[2], camera, y));
    }
    if (isIntersection4) {
      ix.push(LineService.getIntersectionX(wall.lines[3], camera, y));
    }

    let minX: number = ix[0];
    let maxX: number = ix[0];
    ix.forEach(c => {
      if (c < minX) {
        minX = c;
      }
      if (c > maxX) {
        maxX = c;
      }
    });

    return x >= minX && x <= maxX;
  }
}
