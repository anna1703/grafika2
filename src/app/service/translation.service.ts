import { Injectable } from '@angular/core';
import {Point3D} from "../model/point3-d";
import {Constants} from "../model/constants";
import {TranslationType} from "../model/translation-type.enum";
import {Scene} from "../model/scene";

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  public static translate(data: Scene, translationType: TranslationType): Scene {
    switch (translationType) {
      case TranslationType.FORWARD:
        TranslationService.translateForward(data);
        return data;
      case TranslationType.BACKWARD:
        TranslationService.translateBackward(data);
        return data;
      case TranslationType.LEFT:
        TranslationService.translateLeft(data);
        return data;
      case TranslationType.RIGHT:
        TranslationService.translateRight(data);
        return data;
      case TranslationType.DOWN:
        TranslationService.translateDown(data);
        return data;
      case TranslationType.UP:
        TranslationService.translateUp(data);
        return data;
    }
  }

  private static translateForward(data: Scene): void {
    data.shapes.forEach(s => s.changePoints(s.points.map(TranslationService.translateForwardPoint)));
  }

  private static translateForwardPoint(point: Point3D): Point3D {
    return new Point3D(point.x, point.y - Constants.TRANSLATION_STEP, point.z);
  }

  private static translateBackward(data: Scene): void {
    data.shapes.forEach(s => s.changePoints(s.points.map(TranslationService.translateBackwardLine)));
  }

  private static translateBackwardLine(point: Point3D): Point3D {
    return new Point3D(point.x, point.y + Constants.TRANSLATION_STEP, point.z);
  }

  private static translateLeft(data: Scene): void {
    data.shapes.forEach(s => s.changePoints(s.points.map(TranslationService.translateLeftLine)));
  }

  private static translateLeftLine(point: Point3D): Point3D {
    return new Point3D(point.x + Constants.TRANSLATION_STEP, point.y, point.z);
  }

  private static translateRight(data: Scene): void {
    data.shapes.forEach(s => s.changePoints(s.points.map(TranslationService.translateRightLine)));
  }

  private static translateRightLine(point: Point3D): Point3D {
    return new Point3D(point.x - Constants.TRANSLATION_STEP, point.y, point.z);
  }

  private static translateUp(data: Scene): void {
    data.shapes.forEach(s => s.changePoints(s.points.map(TranslationService.translateUpLine)));
  }

  private static translateUpLine(point: Point3D): Point3D {
    return new Point3D(point.x, point.y, point.z - Constants.TRANSLATION_STEP);
  }

  private static translateDown(data: Scene): void {
    data.shapes.forEach(s => s.changePoints(s.points.map(TranslationService.translateDownLine)));
  }

  private static translateDownLine(point: Point3D): Point3D {
    return new Point3D(point.x, point.y, point.z + Constants.TRANSLATION_STEP);
  }
}
