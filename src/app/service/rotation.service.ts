import { Injectable } from '@angular/core';
import {RotationType} from "../model/rotation-type.enum";
import {Point3D} from "../model/point3-d";
import {Constants} from "../model/constants";
import {Scene} from "../model/scene";

@Injectable({
  providedIn: 'root'
})
export class RotationService {

  public static rotate(data: Scene, rotationType: RotationType): Scene {
    switch (rotationType) {
      case RotationType.LEFT:
        RotationService.rotateLeft(data);
        return data;
      case RotationType.RIGHT:
        RotationService.rotateRight(data);
        return data;
      case RotationType.UP:
        RotationService.rotateUp(data);
        return data;
      case RotationType.DOWN:
        RotationService.rotateDown(data);
        return data;
      case RotationType.FORWARD:
        RotationService.rotateForward(data);
        return data;
      case RotationType.BACKWARD:
        RotationService.rotateBackward(data);
        return data;
    }
  }

  private static rotateLeft(data: Scene): void {
    data.shapes.forEach(s => s.changePoints(s.points.map(RotationService.rotateLeftLine)));
  }

  private static rotateLeftLine(point: Point3D): Point3D {
    let x = (point.x * Math.cos(-1 * Constants.ROTATION_STEP)) - (point.y * Math.sin(-1 * Constants.ROTATION_STEP));
    let y = (point.x * Math.sin(-1 * Constants.ROTATION_STEP)) + (point.y * Math.cos(-1 * Constants.ROTATION_STEP));
    return new Point3D(x, y, point.z);
  }

  private static rotateRight(data: Scene): void {
    data.shapes.forEach(s => s.changePoints(s.points.map(RotationService.rotateRightLine)));
  }

  private static rotateRightLine(point: Point3D): Point3D {
    let x = (point.x * Math.cos(Constants.ROTATION_STEP)) - (point.y * Math.sin(Constants.ROTATION_STEP));
    let y = (point.x * Math.sin(Constants.ROTATION_STEP)) + (point.y * Math.cos(Constants.ROTATION_STEP));
    return new Point3D(x, y, point.z);
  }

  private static rotateUp(data: Scene): void {
    data.shapes.forEach(s => s.changePoints(s.points.map(RotationService.rotateUpLine)));
  }

  private static rotateUpLine(point: Point3D): Point3D {
    let x = (point.x * Math.cos(-1 * Constants.ROTATION_STEP)) + (point.z * Math.sin(-1 * Constants.ROTATION_STEP));
    let z = (-1 * point.x * Math.sin(-1 * Constants.ROTATION_STEP)) + (point.z * Math.cos(-1 * Constants.ROTATION_STEP));
    return new Point3D(x, point.y, z);
  }

  private static rotateDown(data: Scene): void {
    data.shapes.forEach(s => s.changePoints(s.points.map(RotationService.rotateDownLine)));
  }

  private static rotateDownLine(point: Point3D): Point3D {
    let x = (point.x * Math.cos(Constants.ROTATION_STEP)) + (point.z * Math.sin(Constants.ROTATION_STEP));
    let z = (-1 * point.x * Math.sin(Constants.ROTATION_STEP)) + (point.z * Math.cos(Constants.ROTATION_STEP));
    return new Point3D(x, point.y, z);
  }

  private static rotateForward(data: Scene): void {
    data.shapes.forEach(s => s.changePoints(s.points.map(RotationService.rotateForwardLine)));
  }

  private static rotateForwardLine(point: Point3D): Point3D {
    let y = (point.y * Math.cos(-1 * Constants.ROTATION_STEP)) - (point.z * Math.sin(-1 * Constants.ROTATION_STEP));
    let z = (point.y * Math.sin(-1 * Constants.ROTATION_STEP)) + (point.z * Math.cos(-1 * Constants.ROTATION_STEP));
    return new Point3D(point.x, y, z);
  }

  private static rotateBackward(data: Scene): void {
    data.shapes.forEach(s => s.changePoints(s.points.map(RotationService.rotateBackwardLine)));
  }

  private static rotateBackwardLine(point: Point3D): Point3D {
    let y = (point.y * Math.cos(Constants.ROTATION_STEP)) - (point.z * Math.sin(Constants.ROTATION_STEP));
    let z = (point.y * Math.sin(Constants.ROTATION_STEP)) + (point.z * Math.cos(Constants.ROTATION_STEP));
    return new Point3D(point.x, y, z);
  }
}

