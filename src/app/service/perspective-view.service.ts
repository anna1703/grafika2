import { Injectable } from '@angular/core';
import {Point2D} from "../model/point2-d";
import {Point3D} from "../model/point3-d";
import {Camera} from "../model/camera";
import {Line3D} from "../model/line3-d";
import {Line2D} from "../model/line2-d";

@Injectable({
  providedIn: 'root'
})
export class PerspectiveViewService {

  public static transformPoint(point: Point3D, camera: Camera): Point2D {
    let rate: number = camera.vpd / (point.y - camera.y);
    let x: number = rate * point.x;
    let y: number = rate * point.z;
    return new Point2D(x, y);
  }

  public static transformLine(line: Line3D, camera: Camera): Line2D {
    let startPoint: Point2D = this.transformPoint(line.startPoint, camera);
    let endPoint: Point2D = this.transformPoint(line.endPoint, camera);
    return new Line2D(startPoint, endPoint);
  }

}
