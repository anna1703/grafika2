import { Injectable } from '@angular/core';
import {Point3D} from "../model/point3-d";
import {Scene} from "../model/scene";
import {Color} from "../model/color.enum";
import {Shape} from "../model/shape";

@Injectable({
  providedIn: 'root'
})
export class DataReaderService {

  public static read(): Scene {
    let scene: Scene = new Scene();
    let points: Array<Point3D>;

    // budynek lewy przod
    points = [];
    points.push(new Point3D(400, 1250, -500));
    points.push(new Point3D(550, 1250, -500));
    points.push(new Point3D(550, 1900, -500));
    points.push(new Point3D(400, 1900, -500));
    points.push(new Point3D(400, 1250, -300));
    points.push(new Point3D(550, 1250, -300));
    points.push(new Point3D(550, 1900, -300));
    points.push(new Point3D(400, 1900, -300));
    scene.addShape(new Shape(points, Color.ORANGE));

    // budynek lewy tyl
    points = [];
    points.push(new Point3D(410, 2000, -500));
    points.push(new Point3D(700, 2000, -500));
    points.push(new Point3D(700, 2550, -500));
    points.push(new Point3D(410, 2550, -500));
    points.push(new Point3D(410, 2000, -100));
    points.push(new Point3D(700, 2000, -100));
    points.push(new Point3D(700, 2550, -100));
    points.push(new Point3D(410, 2550, -100));
    scene.addShape(new Shape(points, Color.RED));

    // budynek prawy przod
    points = [];
    points.push(new Point3D(-380, 1250, -500));
    points.push(new Point3D(-550, 1250, -500));
    points.push(new Point3D(-550, 1650, -500));
    points.push(new Point3D(-380, 1650, -500));
    points.push(new Point3D(-380, 1250, -450));
    points.push(new Point3D(-550, 1250, -450));
    points.push(new Point3D(-550, 1650, -350));
    points.push(new Point3D(-380, 1650, -350));
    scene.addShape(new Shape(points, Color.BLUE));

    // budynek prawy tyl
    points = [];
    points.push(new Point3D(-380, 1850, -500));
    points.push(new Point3D(-550, 1850, -500));
    points.push(new Point3D(-550, 2400, -500));
    points.push(new Point3D(-380, 2400, -500));
    points.push(new Point3D(-380, 1850, 200));
    points.push(new Point3D(-550, 1850, 200));
    points.push(new Point3D(-550, 2400, 200));
    points.push(new Point3D(-380, 2400, 200));
    scene.addShape(new Shape(points, Color.YELLOW));

    // droga
    points = [];
    points.push(new Point3D(-350, 800, -500));
    points.push(new Point3D(350, 800, -500));
    points.push(new Point3D(350, 2550, -500));
    points.push(new Point3D(-350, 2550, -500));
    points.push(new Point3D(-350, 800, -520));
    points.push(new Point3D(350, 800, -520));
    points.push(new Point3D(350, 2550, -520));
    points.push(new Point3D(-350, 2550, -520));
    scene.addShape(new Shape(points, Color.GREEN));

    return scene;
  }

}
