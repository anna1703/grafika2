import {Component, OnInit, ViewChild, ElementRef, Input, OnChanges, SimpleChanges} from '@angular/core';
import {Camera} from "../model/camera";
import {Constants} from "../model/constants";
import {Scene} from "../model/scene";
import {ScanningAlgorithmService} from "../service/scanning-algorithm.service";
import {Color} from "../model/color.enum";
import {CanvasLine} from "../model/canvas-line";

@Component({
  selector: 'gk-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.scss']
})
export class CanvasComponent implements OnInit, OnChanges {

  @ViewChild('canvas')
  canvasRef: ElementRef;

  @Input()
  data: Scene;

  @Input()
  camera: Camera;

  lines: Array<CanvasLine>;

  canvas: any;
  ctx: any;

  constructor(private scanningAlgorithm: ScanningAlgorithmService) { }

  ngOnInit() {
    this.canvas = this.canvasRef['nativeElement'];
    this.ctx = this.canvas.getContext('2d');

    this.canvas.width = Constants.WIDTH;
    this.canvas.height = Constants.HEIGHT;

    let transX: number = this.canvas.width * 0.5;
    let transY: number = this.canvas.height * 0.5;
    this.ctx.translate(transX, transY);
    this.ctx.scale(1, -1);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ((changes['data'] && changes['data'].currentValue) || (changes['camera'] && changes['camera'].currentValue)
        && this.data && this.camera) {

      this.scanningAlgorithm.setValues(this.data, this.camera);
      this.lines = this.scanningAlgorithm.calculate();

      this.clear();
      this.print();
    }
  }

  print(): void {
    this.lines.forEach(line => {
      this.ctx.beginPath();
      this.ctx.moveTo(line.startX, line.y);
      this.ctx.lineTo(line.endX, line.y);
      this.ctx.closePath();
      this.ctx.strokeStyle = this.getStringFromColor(line.color);
      this.ctx.stroke();
    });
  }

  clear(): void {
    // Store the current transformation matrix
    this.ctx.save();
    // Use the identity matrix while clearing the canvas
    this.ctx.setTransform(1, 0, 0, 1, 0, 0);
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    // Restore the transform
    this.ctx.restore();
  }

  private getStringFromColor(color: Color): String {
    switch (color) {
      case Color.BLUE:
        return "blue";
      case Color.GREEN:
        return "green";
      case Color.ORANGE:
        return "orange";
      case Color.RED:
        return "red";
      case Color.WHITE:
        return "white";
      case Color.YELLOW:
        return "yellow";
    }
  }
}
