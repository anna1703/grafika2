export enum RotationType {
  LEFT,
  RIGHT,
  UP,
  DOWN,
  FORWARD,
  BACKWARD
}
