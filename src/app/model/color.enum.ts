export enum Color {
  GREEN,
  RED,
  ORANGE,
  BLUE,
  YELLOW,
  WHITE,
}
