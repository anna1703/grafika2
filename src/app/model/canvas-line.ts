import {Color} from "./color.enum";

export class CanvasLine {
  startX: number;
  endX: number;
  y: number;
  color: Color;

  constructor(startX: number, endX: number, y: number, color: Color) {
    this.startX = startX;
    this.endX = endX;
    this.y = y;
    this.color = color;
  }
}
