import {Color} from "./color.enum";

export class Constants {
  static HEIGHT: number = 500;
  static WIDTH: number = 500;
  static ZOOM_STEP: number = 40;
  static TRANSLATION_STEP: number = 50;
  static ROTATION_STEP: number = Math.PI/10;
  static BACKGROUND_COLOR: Color = Color.WHITE;
}
