import {Constants} from "./constants";

export class Camera {
  public height: number = Constants.HEIGHT;
  public width: number = Constants.WIDTH;
  public vpd: number;
  public y: number;

  constructor(vpd: number, y: number) {
    this.vpd = vpd;
    this.y = y;
  }
}
