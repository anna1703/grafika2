import {Point3D} from "./point3-d";
import {Color} from "./color.enum";
import {Wall} from "./wall";
import {Line3D} from "./line3-d";

export class Shape {
  walls: Array<Wall>;
  lines: Array<Line3D>;
  points: Array<Point3D>;
  color: Color;

  constructor(points: Array<Point3D>, color: Color) {
    this.points = points;
    this.color = color;
    this.calculateLines();
    this.calculateWalls();
  }

  public changePoints(points: Array<Point3D>): void {
    this.points = points;
    this.calculateLines();
    this.calculateWalls();
  }

  private calculateLines(): void {
    this.lines = [];

    this.lines.push(new Line3D(this.points[0], this.points[1]));
    this.lines.push(new Line3D(this.points[1], this.points[2]));
    this.lines.push(new Line3D(this.points[2], this.points[3]));
    this.lines.push(new Line3D(this.points[3], this.points[0]));

    this.lines.push(new Line3D(this.points[4], this.points[5]));
    this.lines.push(new Line3D(this.points[5], this.points[6]));
    this.lines.push(new Line3D(this.points[6], this.points[7]));
    this.lines.push(new Line3D(this.points[7], this.points[4]));

    this.lines.push(new Line3D(this.points[0], this.points[4]));
    this.lines.push(new Line3D(this.points[1], this.points[5]));
    this.lines.push(new Line3D(this.points[2], this.points[6]));
    this.lines.push(new Line3D(this.points[3], this.points[7]));
  }

  private calculateWalls(): void {
    this.walls = [];
    let linesOfWall: Array<Line3D>;

    linesOfWall = [];
    linesOfWall.push(this.lines[0]);
    linesOfWall.push(this.lines[1]);
    linesOfWall.push(this.lines[2]);
    linesOfWall.push(this.lines[3]);
    this.walls.push(new Wall(linesOfWall, this.color));

    linesOfWall = [];
    linesOfWall.push(this.lines[4]);
    linesOfWall.push(this.lines[5]);
    linesOfWall.push(this.lines[6]);
    linesOfWall.push(this.lines[7]);
    this.walls.push(new Wall(linesOfWall, this.color));

    linesOfWall = [];
    linesOfWall.push(this.lines[8]);
    linesOfWall.push(this.lines[4]);
    linesOfWall.push(this.lines[9]);
    linesOfWall.push(this.lines[0]);
    this.walls.push(new Wall(linesOfWall, this.color));

    linesOfWall = [];
    linesOfWall.push(this.lines[9]);
    linesOfWall.push(this.lines[5]);
    linesOfWall.push(this.lines[10]);
    linesOfWall.push(this.lines[1]);
    this.walls.push(new Wall(linesOfWall, this.color));

    linesOfWall = [];
    linesOfWall.push(this.lines[10]);
    linesOfWall.push(this.lines[6]);
    linesOfWall.push(this.lines[11]);
    linesOfWall.push(this.lines[2]);
    this.walls.push(new Wall(linesOfWall, this.color));

    linesOfWall = [];
    linesOfWall.push(this.lines[11]);
    linesOfWall.push(this.lines[7]);
    linesOfWall.push(this.lines[8]);
    linesOfWall.push(this.lines[3]);
    this.walls.push(new Wall(linesOfWall, this.color));
  }

}

