import {Color} from "./color.enum";
import {Line3D} from "./line3-d";

export class Wall {
  lines: Array<Line3D>;
  color: Color;
  A: number;
  B: number;
  C: number;
  D: number;

  constructor(lines: Array<Line3D>, color: Color) {
    this.lines = lines;
    this.color = color;

    let x1: number = lines[0].startPoint.x;
    let y1: number = lines[0].startPoint.y;
    let z1: number = lines[0].startPoint.z;

    let x2: number = lines[0].endPoint.x;
    let y2: number = lines[0].endPoint.y;
    let z2: number = lines[0].endPoint.z;

    let x3: number = lines[1].endPoint.x;
    let y3: number = lines[1].endPoint.y;
    let z3: number = lines[1].endPoint.z;

    this.A = (y2 - y1) * (z3 - z1) - (z2 - z1) * (y3 - y1);
    this.B = (z2 - z1) * (x3 - x1) - (x2 - x1) * (z3 - z1);
    this.C = (x2 - x1) * (y3 - y1) - (y2 - y1) * (x3 - x1);
    this.D = -(this.A * x1 + this.B * y1 + this.C * z1);
  }

}
