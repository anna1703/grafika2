import {Point3D} from "./point3-d";

export class Line3D {
  startPoint: Point3D;
  endPoint: Point3D;

  constructor(startPoint: Point3D, endPoint: Point3D) {
    this.startPoint = startPoint;
    this.endPoint = endPoint;
  }

}
