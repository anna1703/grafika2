import {Point2D} from "./point2-d";

export class Line2D {
  startPoint: Point2D;
  endPoint: Point2D;

  constructor(startPoint: Point2D, endPoint: Point2D) {
    this.startPoint = startPoint;
    this.endPoint = endPoint;
  }
}
