import {Shape} from "./shape";

export class Scene {
  shapes: Array<Shape>;

  constructor() {
    this.shapes = [];
  }

  addShape(shape: Shape): void {
    this.shapes.push(shape);
  }

}
