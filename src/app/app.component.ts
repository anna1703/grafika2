import { Component } from '@angular/core';
import {Camera} from "./model/camera";
import {ZoomType} from "./model/zoom-type.enum";
import {TranslationType} from "./model/translation-type.enum";
import {RotationType} from "./model/rotation-type.enum";
import {TranslationService} from "./service/translation.service";
import {ZoomService} from "./service/zoom.service";
import {RotationService} from "./service/rotation.service";
import {Scene} from "./model/scene";

@Component({
  selector: 'gk-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  scene: Scene;
  camera: Camera;

  loadData(event): void {
    this.scene = event;
    this.camera = new Camera(350, 0);
  }

  doZoom(event: ZoomType): void {
    this.camera = ZoomService.zoom(this.camera, event);
  }

  doTranslation(event: TranslationType): void {
    //noinspection TypeScriptUnresolvedFunction
    this.scene = Object.assign({}, TranslationService.translate(this.scene, event));
  }

  doRotation(event: RotationType): void {
    //noinspection TypeScriptUnresolvedFunction
    this.scene = Object.assign({}, RotationService.rotate(this.scene, event));
  }

}
